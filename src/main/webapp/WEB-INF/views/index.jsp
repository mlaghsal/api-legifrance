<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="fr">
<head>
    <title>Api-legifrance</title>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type' content='text/html; charset=UTF-8" />
    <link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.3.1/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap-select/1.13.8/css/bootstrap-select.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/json-viewer.css" />
    <script language="JavaScript" type="text/javascript" src="webjars/jquery/3.4.0/jquery.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="webjars/popper.js/1.14.7/umd/popper.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="webjars/bootstrap/4.3.1/js/bootstrap.js"></script>
    <script language="JavaScript" type="text/javascript" src="webjars/bootstrap-select/1.13.8/js/bootstrap-select.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/static/js/json-viewer.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">

            <div class="navbar-brand">

                <a href="${pageContext.request.contextPath}">
                    <div class="logo">
                    <img alt="Accueil Légifrance.fr - le service public de la diffusion du droit" src="${pageContext.request.contextPath}/static/img/logo.svg">
                    </div>
                </a>
           </div>
        </div>
        <div>
            <button type="button" class="btn btn-info" id="btn-consultContenue">Consulter un contenu</button>
            <a href="${pageContext.request.contextPath}/logout" class="btn btn-info" role="button">Quitter</a>
        </div>
    </div>
</nav>
<!--div class="container">
    <div class="starter-template">
        <h1>Api Legifrance</h1>
    </div>
</div-->

<div class="container-fluid">
    <div class="search-text">
        <form id="ssearch-form">
            <input type="hidden" id="fond" value="ALL"/>
            <!-- UN_DES_MOTS, EXACTE, TOUS_LES_MOTS_DANS_UN_CHAMP, AUCUN_DES_MOTS -->
            <input type="hidden" id="typeRecherche" value="TOUS_LES_MOTS_DANS_UN_CHAMP"/>
            <!--
             ALL, TITLE, TABLE, NOR, NUM, NUM_DEC, NUM_ARTICLE, ARTICLE, MINISTERE, VISA, NOTICE,
             VISA_NOTICE, TRAVAUX_PREP, SIGNATURE, NOTA, NUM_AFFAIRE, ABSTRATS, RESUMES, TEXTE, ECLI,
             NUM_LOI_DEF, TYPE_DECISION, NUMERO_INTERNE,
             REF_PUBLI, RESUME_CIRC, TEXTE_REF, TITRE_LOI_DEF, RAISON_SOCIALE, MOTS_CLES, IDCC
            -->
            <input type="hidden" id="typeChamp" value="ALL"/>

            <input type="hidden" id="facette1" value=""/>

            <!--input type="hidden" id="facette1" value="ARTICLE_QUESTION_USUELLE"/>
            <input type="hidden" id="valeurs" value='"CHAMP_APPLICATION","CONGES_EXCEPTIONNELS","DUREE","INDEMNITES_LICENCIEMENT","PERIODE_ESSAI","PREAVIS","SALAIRES"'/-->

            <!--input type="hidden" id="facette1" value="NOM_CODE"/>
            <input type="hidden" id="valeurs" value="Code du travail"/-->
            <!-- input type="hidden" id="facette1" value="ARTICLE_QUESTION_USUELLE"/-->
            <!--input type="hidden" id="valeurs" value="CONGES_EXCEPTIONNELS"/>
            <input type="hidden" id="valeurs" value="DUREE"/>
            <input type="hidden" id="valeurs" value="INDEMNITES_LICENCIEMENT"/>
            <input type="hidden" id="valeurs" value="PERIODE_ESSAI"/>
            <input type="hidden" id="valeurs" value="PREAVIS"/>
            <input type="hidden" id="valeurs" value="SALAIRES"/-->


            <!--input type="hidden" id="facette2" value="DATE_VERSION"/-->
            <input type="hidden" id="facette2" value=""/>
            <input type="hidden" id="singleDate" value="1538352000000"/>

            <div class="form-group">
                <label class="control-label">Effectuer une recherche dans :</label>
                <div class="row">
                    <div class="col-md-2">
                        <div class="dropdown">
                            <button id="bt-collapseFond" class="btn btn-primary dropdown-toggle" type="button" data-toggle="collapse"
                                    data-target="#collapseFond" aria-controls="collapseFond">Tous les contenus<span class="caret"></span>
                            </button>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="dropdown">
                            <button id='bt-collapseChamp' class="btn btn-primary dropdown-toggle" type="button" data-toggle="collapse"
                                    data-target="#collapseChamp" aria-controls="collapseChamp">Dans tous les champs<span class="caret"></span>
                            </button>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <input type="text" class="form-control" id="search" placeholder="Rechercher un mot, une expression, une référence ..."/>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" id="bth-search" class="btn btn-primary">Rechercher</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="container-fluid">

    <div class="collapse multi-collapse" id="collapseFond">
        <h4>Sélectionner un fonds</h4>

        <div class="list-group" id="listgroupFond">
            <div class="row">
                <div class="col-4">
                    <button value="CODE_DATE" type="button" class="list-group-item list-group-item-action" title='Codes'>Codes</button>
                    <button value="CIRC" type="button" class="list-group-item list-group-item-action">Circulaires et instructions</button>
                    <button value="JURI" type="button" class="list-group-item list-group-item-action">Jurisprudence judiciaire</button>
                    <button value="CNIL" type="button" class="list-group-item list-group-item-action">CNIL</button>
                </div>
                <div class="col-4">
                    <button value="LODA_DATE" type="button" class="list-group-item list-group-item-action">Lois, ordonnances, décrets, arrêtés</button>
                    <button value="CONSTIT" type="button" class="list-group-item list-group-item-action">Jurisprudence constitutionnelle</button>
                    <button value="KALI" type="button" class="list-group-item list-group-item-action">Accords de branche et conventions collectives</button>
                    <button value="ALL" type="button" class="list-group-item list-group-item-action">Tous les contenus</button>
                </div>
                <div class="col-4">
                    <button value="JORF" type="button" class="list-group-item list-group-item-action">Journal officiel</button>
                    <button value="CETAT" type="button" class="list-group-item list-group-item-action">Jurisprudence administrative</button>
                    <button value="ACCO" type="button" class="list-group-item list-group-item-action">Accords d'entreprise</button>
                </div>
            </div>
        </div>
    </div>

    <div class="collapse multi-collapse" id="collapseChamp">
        <div class="list-group" id="listgroupChamp">
            <div class="row">
                <div class="col-4">
                    <button value="ALL" type="button" class="list-group-item list-group-item-action">Dans tous les champs</button>
                </div>
                <div class="col-4">
                    <button value="TITLE" type="button" class="list-group-item list-group-item-action">Dans les titres</button>
                </div>
            </div>
        </div>
    </div>

</div>

</div>

<div class="container-fluid">
    <div class="starter-template">
        <div id="feedbackSearch"></div>
        <div class="resultat" id="jsonResponseSearch"></div>
    </div>
</div>

<div>&nbsp;</div>
<!-- consulter le contenu -->

<div id="content" class="container-fluid d-none">
    <div class="search-text">
        <form id="consult-form">

            <div class="form-group">
                <label class="control-label">Consulter un contenu :</label>
                <div class="row">
                    <div class="col-md-4">
                        <select id="typeContenue" title="Sélectionner un fonds" class="selectpicker" multiple data-max-options="1" onchange="displayFormContent(this)">
                            <optgroup label="Texte">
                                <option data-subtext="/consult/jorf" value="texteJORF">Contenu texte fonds JORF</option>
                                <option data-subtext="/consult/legiPart" value="texteLEGI">Contenu texte fonds LEGI</option>
                                <option data-subtext="/consult/lawDecree" value="texteLODA">Contenu texte type LODA</option>
                                <option data-subtext="/consult/acco" value="accord">Contenu d'un accord d'entreprise</option>
                                <option data-subtext="/consult/juri" value="juri">Contenu texte Jurisprudence</option>
                                <option data-subtext="consult/kaliText" value="kali">Contenu des conventions collectives</option>
                                <option data-subtext="/consult/circulaire" value="circ">Contenu d'une circualire</option>
                                <option data-subtext="/consult/cnil" value="cnil">Contenu texte fonds CNIL</option>
                            </optgroup>
                            <optgroup label="Codes">
                                <option data-subtext="/consult/code/tableMatieres" value="tdmCode">Contenu table des matières d'un CODE</option>
                                <option data-subtext="/consult/code" value="code">Contenu texte type CODE</option>
                            </optgroup>
                            <optgroup label="Article">
                                <option data-subtext="/consult/getArticle" value="article">Contenu d'un article</option>
                                <option data-subtext="/consult/kaliArticle" value="kaliArticle">Contenu des conventions collectives depuis un article</option>
                            </optgroup>
                        </select>
                    </div>

                    <div class="col-md-3">
                        <input type="text" class="form-control" id="texteId" placeholder="Identifiant texte ou article"/>
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control d-none" id="sectionId" placeholder="Chronical ID de la section"/>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" id="bth-consult" class="btn btn-primary">Consulter</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="starter-template">
        <div id="feedbackContent"></div>
        <div class="resultat" id="jsonResponseContent"></div>
    </div>
</div>


<script type="text/javascript">
    $('#listgroupFond').on('click', function (e) {
        // alert(e.target.value);

        $('#collapseFond').collapse('hide');
        $('#bt-collapseFond').text(e.target.innerText);
        $('#fond').val(e.target.value);

    });
    $('#collapseChamp').on('click', function (e) {
        $('#collapseChamp').collapse('hide');
        $('#bt-collapseChamp').text(e.target.innerText);
        $('#typeChamp').val(e.target.value);
    });
    $(function () {

        var jsonViewerSearch = new JSONViewer();
        document.querySelector('#jsonResponseSearch').appendChild(jsonViewerSearch.getContainer());

        var jsonViewerContent = new JSONViewer();
        document.querySelector('#jsonResponseContent').appendChild(jsonViewerContent.getContainer());

        $("#ssearch-form").submit(function () {

            $('#feedbackSearch').html("<b>Recherche...</b>");

            if($("#fond").val() == "CODE_DATE"){
                $("#facette1").val("NOM_CODE");
                $("#facette2").val("DATE_VERSION");
            }else{
                $("#facette1").val("");
                $("#facette2").val("");
            }

            var jsonObject = {
                fond: $("#fond").val(),
                search: $("#search").val(),
                typeRecherche: $("#typeRecherche").val(),
                typeChamp: $("#typeChamp").val(),
                facette1: $("#facette1").val(),
                valeurs: $("#valeurs").val(),
                facette2: $("#facette2").val(),
                singleDate: $("#singleDate").val()
            };
//  alert(JSON.stringify(jsonObject));
            $.ajax({
                url: '${pageContext.request.contextPath}/search',
                dataType: 'json',
                type: 'post',
                contentType: 'application/json',
                data: JSON.stringify(jsonObject),
                timeout: 600000,
                processData: true
            }).done(function(data){
                $('#feedbackSearch').html("<b>Nombre de résultat : " + data.totalResultNumber + "</b>");
                jsonViewerSearch.showJSON(data, null, 3);
            })
                    .fail(function() {
                        $('#feedbackSearch').html("<b>Erreur : Posting failed</b>");
                    });
            return false;
        });


        $("#consult-form").submit(function () {

            $('#feedbackContent').html("<b>Recherche...</b>");

            var jsonObject = {
                type: $("#typeContenue").val()[0],
                id: $("#texteId").val(),
                sectionId: $("#sectionId").val()
            };

            $.ajax({
                url: '${pageContext.request.contextPath}/consult/content',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(jsonObject),
                timeout: 600000,
                processData: true
            }).done(function(data){
                $('#feedbackContent').html("<b>Résultat</b>");
                jsonViewerContent.showJSON(data, null, 3);
            })
                    .fail(function(e) {
                        $('#feedbackContent').html("<b>Erreur : Posting failed</b>");
                        //jsonViewerContent.showJSON(JSON.parse(e.responseText), null, 3);
                    });
            return false;
        })
    });

    function displayFormContent(elem){
        if(elem.value == "code") {
            $("#sectionId").removeClass('d-none');
        } else {
            $("#sectionId").addClass('d-none');
        }
    };

    $('#btn-consultContenue').on('click', function(event) {
        $("#content").removeClass('d-none');
    });
</script>

</body>
</html>
