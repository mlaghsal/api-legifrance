<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="fr">
<head>
    <title>Login Page</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.3.1/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/main.css" />
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <div class="navbar-brand">
                <img src="${pageContext.request.contextPath}/static/img/logo.png" alt="Api-legifrance" role="presentation">
            </div>
        </div>
    </div>
    <hr class="my-4">
</nav>
<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <h5 class="card-title text-center">Ouvrez une session pour continuer.</h5>

                    <c:if test="${param.error != null}">
                        <p style='color:red'>
                            Invalid username and password.
                        </p>
                    </c:if>
                    <c:if test="${param.logout != null}">
                        <p style='color:blue'>
                            You have been logged out.
                        </p>
                    </c:if>

                    <form class="form-signin" action="login" method="post">
                        <div class="form-label-group">
                            <input type="input" id="username" name="username" class="form-control" placeholder="username" required autofocus>
                            <label for="username">Nom d'utilisateur</label>
                        </div>

                        <div class="form-label-group">
                            <input type="password" id="password" class="form-control" name="password" placeholder="Password" required>
                            <label for="password">Mot de passe</label>
                        </div>

                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Ouvrez une session</button>
                        <hr class="my-4">

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>