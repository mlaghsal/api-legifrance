package fr.dila.gouv.api.legifrance.dto.request;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "articleCid",
        "articleNum",
        "date",
        "textCid"
})
public class SameNumArticleRequest {
    @JsonProperty("articleCid")
    private String articleCid = "LEGIARTI000006419319";
    @JsonProperty("articleNum")
    private Integer articleNum = 16;
    @JsonProperty("date")
    private String date = "2019-03-22";
    @JsonProperty("textCid")
    private String textCid = "LEGITEXT000006070721";

    @JsonProperty("articleCid")
    public String getArticleCid() {
        return articleCid;
    }

    @JsonProperty("articleCid")
    public void setArticleCid(String articleCid) {
        this.articleCid = articleCid;
    }

    @JsonProperty("articleNum")
    public Integer getArticleNum() {
        return articleNum;
    }

    @JsonProperty("articleNum")
    public void setArticleNum(Integer articleNum) {
        this.articleNum = articleNum;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("textCid")
    public String getTextCid() {
        return textCid;
    }

    @JsonProperty("textCid")
    public void setTextCid(String textCid) {
        this.textCid = textCid;
    }
}
