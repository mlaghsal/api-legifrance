package fr.dila.gouv.api.legifrance.form;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import fr.dila.gouv.api.legifrance.dto.request.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "fond",
        "search",
        "typeRecherche",
        "typeChamp",
        "facette1",
        "valeurs",
        "facette2",
        "singleDate"
})
public class Formsearch{

    private static final Logger logger = LoggerFactory.getLogger(Formsearch.class);
    @JsonProperty("fond")
    private String fond;

    @JsonProperty("search")
    private String search;

    @JsonProperty("typeRecherche")
    private String typeRecherche = "TOUS_LES_MOTS_DANS_UN_CHAMP";

    @JsonProperty("typeChamp")
    private String typeChamp = "ALL";

    @JsonProperty("facette1")
    private String facette1;

    @JsonProperty("valeurs")
    private List<String> valeurs;

    @JsonProperty("facette2")
    private String facette2;

    @JsonProperty("singleDate")
    private String singleDate;

    @JsonProperty("fond")
    public String getFond() {
        return fond;
    }
    @JsonProperty("fond")
    public void setFond(String fond) {
        this.fond = fond;
    }

    @JsonProperty("search")
    public String getSearch() {
        return search;
    }
    @JsonProperty("search")
    public void setSearch(String  search) {
        this.search = search;
    }

    public String getTypeRecherche() {
        return typeRecherche;
    }

    public void setTypeRecherche(String typeRecherche) {
        this.typeRecherche = typeRecherche;
    }

    public String getTypeChamp() {
        return typeChamp;
    }

    public void setTypeChamp(String typeChamp) {
        this.typeChamp = typeChamp;
    }

    public String getFacette1() {
        return facette1;
    }

    public void setFacette1(String facette1) {
        this.facette1 = facette1;
    }

    public List<String> getValeurs() {
        return valeurs;
    }

    public void setValeurs(List<String> valeurs) {
        this.valeurs = valeurs;
    }

    public String getFacette2() {
        return facette2;
    }

    public void setFacette2(String facette2) {
        this.facette2 = facette2;
    }

    public String getSingleDate() {
        return singleDate;
    }

    public void setSingleDate(String singleDate) {
        this.singleDate = singleDate;
    }


    @JsonIgnore
    public SearchRequest getSearchRequest(){

        Critere critere = new Critere();
        critere.setOperateur("ET");
        //critere.setProximite(0);
        /*
        UN_DES_MOTS, EXACTE, TOUS_LES_MOTS_DANS_UN_CHAMP, AUCUN_DES_MOTS
         */
        critere.setTypeRecherche(this.getTypeRecherche());
        critere.setValeur(this.getSearch());

        List<Critere> criteres = new ArrayList<>();
        criteres.add(critere);

        Champ champ = new Champ();
        champ.setOperateur("ET");

        /*
         ALL, TITLE, TABLE, NOR, NUM, NUM_DEC, NUM_ARTICLE, ARTICLE, MINISTERE, VISA, NOTICE,
         VISA_NOTICE, TRAVAUX_PREP, SIGNATURE, NOTA, NUM_AFFAIRE, ABSTRATS, RESUMES, TEXTE, ECLI,
         NUM_LOI_DEF, TYPE_DECISION, NUMERO_INTERNE,
         REF_PUBLI, RESUME_CIRC, TEXTE_REF, TITRE_LOI_DEF, RAISON_SOCIALE, MOTS_CLES, IDCC
         */
        champ.setTypeChamp(this.getTypeChamp()); // TITLE  ALL IDCC
        champ.setCriteres(criteres);

        List<Champ> champs = new ArrayList<>();
        champs.add(champ);

        Search search = new Search();

        search.setOperateur("ET");
        search.setPageNumber(1);
        search.setPageSize(10);
        search.setChamps(champs);

        List<FiltreSearch> filtreSearchList = new ArrayList<>();
        if(this.getFacette1() != null && !this.getFacette1().isEmpty()){
            final FiltreSearch filtreSearch = new FiltreSearch();
            filtreSearch.setFacette(this.getFacette1());
            filtreSearch.setValeurs(this.getValeurs());
            filtreSearchList.add(filtreSearch);
        }


        if(this.getFacette2() != null && !this.getFacette2().isEmpty()) {
            final FiltreSearch filtreSearch2 = new FiltreSearch();
            filtreSearch2.setFacette(this.getFacette2());
            filtreSearch2.setSingleDate(new Date());
            filtreSearchList.add(filtreSearch2);
        }

        if (!filtreSearchList.isEmpty()) {
            search.setFiltres(filtreSearchList);
        }

        //search.setSort("SIGNATURE_DATE_DESC");
        search.setSort("PERTINENCE");
        //search.setTypePagination("DEFAUT");

        SearchRequest searchRequest = new SearchRequest();
        /*
        JORF, CNIL, CETAT, JURI, JUFI, CONSTIT, KALI, CODE_DATE, CODE_ETAT, LODA_DATE, LODA_ETAT, ALL, CIRC, ACCO
         */
        searchRequest.setFond(this.getFond());
        searchRequest.setSearch(search);

        return searchRequest;
    }
}
