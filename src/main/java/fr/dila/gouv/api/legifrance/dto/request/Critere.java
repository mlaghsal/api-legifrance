package fr.dila.gouv.api.legifrance.dto.request;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "operateur",
        "proximite",
        "typeRecherche",
        "valeur"
})
public class Critere {

    @JsonProperty("operateur")
    private String operateur = "ET";
    @JsonProperty("proximite")
    private Integer proximite;
    @JsonProperty("typeRecherche")
    private String typeRecherche;
    @JsonProperty("valeur")
    private String valeur;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("operateur")
    public String getOperateur() {
        return operateur;
    }

    @JsonProperty("operateur")
    public void setOperateur(String operateur) {
        this.operateur = operateur;
    }

    @JsonProperty("proximite")
    public Integer getProximite() {
        return proximite;
    }

    @JsonProperty("proximite")
    public void setProximite(Integer proximite) {
        this.proximite = proximite;
    }

    @JsonProperty("typeRecherche")
    public String getTypeRecherche() {
        return typeRecherche;
    }

    @JsonProperty("typeRecherche")
    public void setTypeRecherche(String typeRecherche) {
        this.typeRecherche = typeRecherche;
    }

    @JsonProperty("valeur")
    public String getValeur() {
        return valeur;
    }

    @JsonProperty("valeur")
    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}