package fr.dila.gouv.api.legifrance.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "end",
        "start"
})
public class DatePeriod {
    @JsonProperty("end")
    private String end = "2019-12-31";
    @JsonProperty("start")
    private String start = "2018-01-01";

    public DatePeriod(String end, String start) {
        this.end = end;
        this.start = start;
    }

    @JsonProperty("end")
    public String getEnd() {
        return end;
}

    @JsonProperty("end")
    public void setEnd(String end) {
        this.end = end;
    }

    @JsonProperty("start")
    public String getStart() {
        return start;
    }

    @JsonProperty("start")
    public void setStart(String start) {
        this.start = start;
    }
}
