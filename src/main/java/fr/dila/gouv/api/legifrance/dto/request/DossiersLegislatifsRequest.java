package fr.dila.gouv.api.legifrance.dto.request;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "legislatureId",
        "type"
})
public class DossiersLegislatifsRequest {
    @JsonProperty("legislatureId")
    private Integer legislatureId = 15;
    @JsonProperty("type")
    private String type = "LOI_PUBLIEE";

    @JsonProperty("legislatureId")
    public Integer getLegislatureId() {
        return legislatureId;
    }

    @JsonProperty("legislatureId")
    public void setLegislatureId(Integer legislatureId) {
        this.legislatureId = legislatureId;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

}
