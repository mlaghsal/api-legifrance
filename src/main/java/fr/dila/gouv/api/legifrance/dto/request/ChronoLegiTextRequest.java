package fr.dila.gouv.api.legifrance.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "dateConsult",
        "endYear",
        "startYear",
        "textCid"
})
public class ChronoLegiTextRequest {
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    @JsonProperty("dateConsult")
    private Date dateConsult = new Date();
    @JsonProperty("endYear")
    private Integer endYear = 2018;
    @JsonProperty("startYear")
    private Integer startYear = 2015;
    @JsonProperty("textCid")
    private String textCid = "LEGITEXT000006070721";

    @JsonProperty("dateConsult")
    public Date getDateConsult() {
        return dateConsult;
    }

    @JsonProperty("dateConsult")
    public void setDateConsult(Date dateConsult) {
        this.dateConsult = dateConsult;
    }

    @JsonProperty("endYear")
    public Integer getEndYear() {
        return endYear;
    }

    @JsonProperty("endYear")
    public void setEndYear(Integer endYear) {
        this.endYear = endYear;
    }

    @JsonProperty("startYear")
    public Integer getStartYear() {
        return startYear;
    }

    @JsonProperty("startYear")
    public void setStartYear(Integer startYear) {
        this.startYear = startYear;
    }

    @JsonProperty("textCid")
    public String getTextCid() {
        return textCid;
    }

    @JsonProperty("textCid")
    public void setTextCid(String textCid) {
        this.textCid = textCid;
    }

}
