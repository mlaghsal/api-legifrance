package fr.dila.gouv.api.legifrance.dto.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "articleVersion",
        "cid",
        "comporteLiens",
        "conditionDiffere",
        "content",
        "dereferenced",
        "etat",
        "executionTime",
        "historique",
        "id",
        "intOrdre",
        "modificatorCid",
        "modificatorDate",
        "modificatorTitle",
        "nota",
        "num",
        "surtitre",
        "type"
})
public class Articles {

    @JsonProperty("articleVersion")
    private String articleVersion;
    @JsonProperty("cid")
    private String cid;
    @JsonProperty("comporteLiens")
    private Boolean comporteLiens;
    @JsonProperty("conditionDiffere")
    private String conditionDiffere;
    @JsonProperty("content")
    private String content;
    @JsonProperty("dereferenced")
    private Boolean dereferenced;
    @JsonProperty("etat")
    private String etat;
    @JsonProperty("executionTime")
    private Integer executionTime;
    @JsonProperty("historique")
    private String historique;
    @JsonProperty("id")
    private String id;
    @JsonProperty("intOrdre")
    private Integer intOrdre;
    @JsonProperty("modificatorCid")
    private String modificatorCid;
    @JsonProperty("modificatorDate")
    private String modificatorDate;
    @JsonProperty("modificatorTitle")
    private String modificatorTitle;
    @JsonProperty("nota")
    private String nota;
    @JsonProperty("num")
    private Integer num;
    @JsonProperty("surtitre")
    private String surtitre;
    @JsonProperty("type")
    private String type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("articleVersion")
    public String getArticleVersion() {
        return articleVersion;
    }

    @JsonProperty("articleVersion")
    public void setArticleVersion(String articleVersion) {
        this.articleVersion = articleVersion;
    }

    @JsonProperty("cid")
    public String getCid() {
        return cid;
    }

    @JsonProperty("cid")
    public void setCid(String cid) {
        this.cid = cid;
    }

    @JsonProperty("comporteLiens")
    public Boolean getComporteLiens() {
        return comporteLiens;
    }

    @JsonProperty("comporteLiens")
    public void setComporteLiens(Boolean comporteLiens) {
        this.comporteLiens = comporteLiens;
    }

    @JsonProperty("conditionDiffere")
    public String getConditionDiffere() {
        return conditionDiffere;
    }

    @JsonProperty("conditionDiffere")
    public void setConditionDiffere(String conditionDiffere) {
        this.conditionDiffere = conditionDiffere;
    }

    @JsonProperty("content")
    public String getContent() {
        return content;
    }

    @JsonProperty("content")
    public void setContent(String content) {
        this.content = content;
    }

    @JsonProperty("dereferenced")
    public Boolean getDereferenced() {
        return dereferenced;
    }

    @JsonProperty("dereferenced")
    public void setDereferenced(Boolean dereferenced) {
        this.dereferenced = dereferenced;
    }

    @JsonProperty("etat")
    public String getEtat() {
        return etat;
    }

    @JsonProperty("etat")
    public void setEtat(String etat) {
        this.etat = etat;
    }

    @JsonProperty("executionTime")
    public Integer getExecutionTime() {
        return executionTime;
    }

    @JsonProperty("executionTime")
    public void setExecutionTime(Integer executionTime) {
        this.executionTime = executionTime;
    }

    @JsonProperty("historique")
    public String getHistorique() {
        return historique;
    }

    @JsonProperty("historique")
    public void setHistorique(String historique) {
        this.historique = historique;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("intOrdre")
    public Integer getIntOrdre() {
        return intOrdre;
    }

    @JsonProperty("intOrdre")
    public void setIntOrdre(Integer intOrdre) {
        this.intOrdre = intOrdre;
    }

    @JsonProperty("modificatorCid")
    public String getModificatorCid() {
        return modificatorCid;
    }

    @JsonProperty("modificatorCid")
    public void setModificatorCid(String modificatorCid) {
        this.modificatorCid = modificatorCid;
    }

    @JsonProperty("modificatorDate")
    public String getModificatorDate() {
        return modificatorDate;
    }

    @JsonProperty("modificatorDate")
    public void setModificatorDate(String modificatorDate) {
        this.modificatorDate = modificatorDate;
    }

    @JsonProperty("modificatorTitle")
    public String getModificatorTitle() {
        return modificatorTitle;
    }

    @JsonProperty("modificatorTitle")
    public void setModificatorTitle(String modificatorTitle) {
        this.modificatorTitle = modificatorTitle;
    }

    @JsonProperty("nota")
    public String getNota() {
        return nota;
    }

    @JsonProperty("nota")
    public void setNota(String nota) {
        this.nota = nota;
    }

    @JsonProperty("num")
    public Integer getNum() {
        return num;
    }

    @JsonProperty("num")
    public void setNum(Integer num) {
        this.num = num;
    }

    @JsonProperty("surtitre")
    public String getSurtitre() {
        return surtitre;
    }

    @JsonProperty("surtitre")
    public void setSurtitre(String surtitre) {
        this.surtitre = surtitre;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}