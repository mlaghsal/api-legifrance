package fr.dila.gouv.api.legifrance.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.dila.gouv.api.legifrance.api.ApiRest;
import fr.dila.gouv.api.legifrance.dto.request.SearchRequest;
import fr.dila.gouv.api.legifrance.form.FormContent;
import fr.dila.gouv.api.legifrance.form.FormRequest;
import fr.dila.gouv.api.legifrance.form.Formsearch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
public class LegiService {
    private static final Logger logger = LoggerFactory.getLogger(LegiService.class);

    @Autowired
    private ApiRest api;

    @Value("${api.host}")
    private String apiHost;

    /**
     * Permet de faire une recherche de documents. On peut préciser le fond, les filtres textuels,
     * les facettes et la pagination
     */
    public ResponseEntity<String> search(Formsearch formsearch) {
        final String apiUrl = apiHost + "/search";
        SearchRequest request = formsearch.getSearchRequest();
        printRequete(request);
        ResponseEntity<String> response = api.doPost(request, apiUrl);
        //logger.debug(response.getBody());
        return response;
    }


    /**
     * Récupère le contenu d’un texte, article, ... à partir de son identifiant
     */
    public ResponseEntity<String> content(FormContent form) {
        FormRequest formRequest = new FormRequest<>();
        Object request = formRequest.getRequest(form);
        printRequete(request);
        final String apiUrl = apiHost + formRequest.getPathApi();
        final ResponseEntity<String> response = api.doPost(request, apiUrl);
        //logger.debug(response.getBody());

        return response;
    }

    private void printRequete(Object request){

        ObjectMapper mapper = new ObjectMapper();
        try {
            String jsonString = mapper.writeValueAsString(request);
            logger.debug(jsonString);
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage());
        }

    }
}
