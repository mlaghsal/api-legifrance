package fr.dila.gouv.api.legifrance.dto.response;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.springframework.util.StringUtils;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "titles",
        "conforme",
        "date",
        "sections",
        "dateDiffusion",
        "datePublication",
        "dateSignature",
        "etat",
        "idAttachment",
        "jorfText",
        "moreArticle",
        "nature",
        "nor",
        "num",
        "numParution",
        "origin",
        "raisonSociale",
        "reference",
        "sizeAttachment",
        "text",
        "type"
})
public class Result {

    @JsonProperty("titles")
    private List<Title> titles = null;
    @JsonProperty("conforme")
    private Boolean conforme;
    @JsonProperty("date")
    private String date;
    @JsonProperty("dateDiffusion")
    private String dateDiffusion;
    @JsonProperty("datePublication")
    private String datePublication;
    @JsonProperty("dateSignature")
    private String dateSignature;
    @JsonProperty("etat")
    private String etat;
    @JsonProperty("idAttachment")
    private String idAttachment;
    @JsonProperty("jorfText")
    private String jorfText;
    @JsonProperty("moreArticle")
    private Boolean moreArticle;
    @JsonProperty("nature")
    private String nature;
    @JsonProperty("nor")
    private String nor;
    @JsonProperty("num")
    private String num;
    @JsonProperty("numParution")
    private String numParution;
    @JsonProperty("origin")
    private String origin;
    @JsonProperty("raisonSociale")
    private String raisonSociale;
    @JsonProperty("reference")
    private String reference;
    @JsonProperty("sizeAttachment")
    private String sizeAttachment;
    @JsonProperty("text")
    private String text;
    @JsonProperty("type")
    private String type;
    @JsonProperty("sections")
    private List<Sections> sections = null;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("titles")
    public List<Title> getTitles() {
        return titles;
    }

    @JsonProperty("titles")
    public void setTitles(List<Title> titles) {
        this.titles = titles;
    }

    @JsonProperty("conforme")
    public Boolean getConforme() {
        return conforme;
    }

    @JsonProperty("conforme")
    public void setConforme(Boolean conforme) {
        this.conforme = conforme;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("dateDiffusion")
    public String getDateDiffusion() {
        return dateDiffusion;
    }

    @JsonProperty("dateDiffusion")
    public void setDateDiffusion(String dateDiffusion) {
        this.dateDiffusion = dateDiffusion;
    }

    @JsonProperty("datePublication")
    public String getDatePublication() {
        return datePublication;
    }

    @JsonProperty("datePublication")
    public void setDatePublication(String datePublication) {
        this.datePublication = datePublication;
    }

    @JsonProperty("dateSignature")
    public String getDateSignature() {
        return dateSignature;
    }

    @JsonProperty("dateSignature")
    public void setDateSignature(String dateSignature) {
        this.dateSignature = dateSignature;
    }

    @JsonProperty("etat")
    public String getEtat() {
        return etat;
    }

    @JsonProperty("etat")
    public void setEtat(String etat) {
        this.etat = etat;
    }

    @JsonProperty("idAttachment")
    public String getIdAttachment() {
        return idAttachment;
    }

    @JsonProperty("idAttachment")
    public void setIdAttachment(String idAttachment) {
        this.idAttachment = idAttachment;
    }

    @JsonProperty("jorfText")
    public String getJorfText() {
        return jorfText;
    }

    @JsonProperty("jorfText")
    public void setJorfText(String jorfText) {
        this.jorfText = jorfText;
    }

    @JsonProperty("moreArticle")
    public Boolean getMoreArticle() {
        return moreArticle;
    }

    @JsonProperty("moreArticle")
    public void setMoreArticle(Boolean moreArticle) {
        this.moreArticle = moreArticle;
    }

    @JsonProperty("nature")
    public String getNature() {
        return nature;
    }

    @JsonProperty("nature")
    public void setNature(String nature) {
        this.nature = nature;
    }

    @JsonProperty("nor")
    public String getNor() {
        return nor;
    }

    @JsonProperty("nor")
    public void setNor(String nor) {
        this.nor = nor;
    }

    @JsonProperty("num")
    public String getNum() {
        return num;
    }

    @JsonProperty("num")
    public void setNum(String num) {
        this.num = num;
    }

    @JsonProperty("numParution")
    public String getNumParution() {
        return numParution;
    }

    @JsonProperty("numParution")
    public void setNumParution(String numParution) {
        this.numParution = numParution;
    }

    @JsonProperty("origin")
    public String getOrigin() {
        return origin;
    }

    @JsonProperty("origin")
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    @JsonProperty("raisonSociale")
    public String getRaisonSociale() {
        return raisonSociale;
    }

    @JsonProperty("raisonSociale")
    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    @JsonProperty("reference")
    public String getReference() {
        return reference;
    }

    @JsonProperty("reference")
    public void setReference(String reference) {
        this.reference = reference;
    }

    @JsonProperty("sizeAttachment")
    public String getSizeAttachment() {
        return sizeAttachment;
    }

    @JsonProperty("sizeAttachment")
    public void setSizeAttachment(String sizeAttachment) {
        this.sizeAttachment = sizeAttachment;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("sections")
    public List<Sections> getSections() {
        return sections;
    }

    @JsonProperty("sections")
    public void setSections(List<Sections> sections) {
        this.sections = sections;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}