package fr.dila.gouv.api.legifrance.dto.request;
import java.util.Arrays;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "years"
})
public class DocsAdminsListRequest {
    @JsonProperty("years")
    private List<Integer> years = Arrays.asList(2016, 2017);

    @JsonProperty("years")
    public List<Integer> getYears() {
        return years;
    }

    @JsonProperty("years")
    public void setYears(List<Integer> years) {
        this.years = years;
    }
}
