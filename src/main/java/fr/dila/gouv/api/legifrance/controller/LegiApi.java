package fr.dila.gouv.api.legifrance.controller;

import fr.dila.gouv.api.legifrance.form.FormContent;
import fr.dila.gouv.api.legifrance.form.Formsearch;
import fr.dila.gouv.api.legifrance.services.LegiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class LegiApi {

    private static final Logger logger = LoggerFactory.getLogger(LegiApi.class);

    @Autowired
    private LegiService service;

    /**
     * Permet de faire une recherche de documents
     * @param formsearch
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    @ResponseBody
    public String search(@RequestBody Formsearch formsearch) {
        return service.search(formsearch).getBody();
    }

    /**
     * Permet de consulter le contenu d'un texte ou d'un article
     * @param form
     * @return
     */
    @RequestMapping(value = "/consult/content", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    @ResponseBody
    public String content(@RequestBody FormContent form) {
        return service.content(form).getBody();
    }
}
