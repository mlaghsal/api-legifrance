package fr.dila.gouv.api.legifrance.dto.request;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "date",
        "end",
        "highlightActivated",
        "id",
        "num",
        "pageNumber",
        "pageSize",
        "searchText",
        "start"
})
public class JorfContConsultRequest {
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    @JsonProperty("date")
    private Date date = new Date();
    @JsonProperty("end")
    private ConsultDateRequest end = new ConsultDateRequest(1, 1, 2019);
    @JsonProperty("highlightActivated")
    private Boolean highlightActivated = true;
    @JsonProperty("id")
    private String id = "JORFCONT000022470431";
    @JsonProperty("num")
    private String num = "0022";
    @JsonProperty("pageNumber")
    private Integer pageNumber = 1;
    @JsonProperty("pageSize")
    private Integer pageSize = 10;
    @JsonProperty("searchText")
    private String searchText = "mariage";
    @JsonProperty("start")
    private ConsultDateRequest start = new ConsultDateRequest(1, 1, 2017);;

    @DateTimeFormat(pattern = "yyyy-mm-dd")
    @JsonProperty("date")
    public Date getDate() {
        return date;
    }

    @DateTimeFormat(pattern = "yyyy-mm-dd")
    @JsonProperty("date")
    public void setDate(Date date) {
        this.date = date;
    }

    @JsonProperty("end")
    public ConsultDateRequest getEnd() {
        return end;
    }

    @JsonProperty("end")
    public void setEnd(ConsultDateRequest end) {
        this.end = end;
    }

    @JsonProperty("highlightActivated")
    public Boolean getHighlightActivated() {
        return highlightActivated;
    }

    @JsonProperty("highlightActivated")
    public void setHighlightActivated(Boolean highlightActivated) {
        this.highlightActivated = highlightActivated;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("num")
    public String getNum() {
        return num;
    }

    @JsonProperty("num")
    public void setNum(String num) {
        this.num = num;
    }

    @JsonProperty("pageNumber")
    public Integer getPageNumber() {
        return pageNumber;
    }

    @JsonProperty("pageNumber")
    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    @JsonProperty("pageSize")
    public Integer getPageSize() {
        return pageSize;
    }

    @JsonProperty("pageSize")
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @JsonProperty("searchText")
    public String getSearchText() {
        return searchText;
    }

    @JsonProperty("searchText")
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    @JsonProperty("start")
    public ConsultDateRequest getStart() {
        return start;
    }

    @JsonProperty("start")
    public void setStart(ConsultDateRequest start) {
        this.start = start;
    }
}
