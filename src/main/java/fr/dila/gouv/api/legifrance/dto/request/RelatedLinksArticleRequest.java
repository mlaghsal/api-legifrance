package fr.dila.gouv.api.legifrance.dto.request;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "articleId"
})
public class RelatedLinksArticleRequest {
    @JsonProperty("articleId")
    private String articleId = "LEGIARTI000032207188";

    @JsonProperty("articleId")
    public String getArticleId() {
        return articleId;
    }

    @JsonProperty("articleId")
    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }
}
