package fr.dila.gouv.api.legifrance.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "dayOfMonth",
        "month",
        "year"
})
public class ConsultDateRequest {
    @JsonProperty("dayOfMonth")
    private Integer dayOfMonth = 1;
    @JsonProperty("month")
    private Integer month = 1;
    @JsonProperty("year")
    private Integer year = 2019;

    public ConsultDateRequest(Integer dayOfMonth, Integer month, Integer year) {
        this.dayOfMonth = dayOfMonth;
        this.month = month;
        this.year = year;
    }

    @JsonProperty("dayOfMonth")
    public Integer getDayOfMonth() {
        return dayOfMonth;
    }

    @JsonProperty("dayOfMonth")
    public void setDayOfMonth(Integer dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    @JsonProperty("month")
    public Integer getMonth() {
        return month;
    }

    @JsonProperty("month")
    public void setMonth(Integer month) {
        this.month = month;
    }

    @JsonProperty("year")
    public Integer getYear() {
        return year;
    }

    @JsonProperty("year")
    public void setYear(Integer year) {
        this.year = year;
    }
}
