package fr.dila.gouv.api.legifrance.dto.response;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "title",
        "legalStatus",
        "dateVersion",
        "dateDebut",
        "dateFin",
        "searchFieldName",
        "values",
        "type"
})
public class Extract {

    @JsonProperty("id")
    private String id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("legalStatus")
    private String legalStatus;
    @JsonProperty("dateVersion")
    private Object dateVersion;
    @JsonProperty("dateDebut")
    private Date dateDebut;
    @JsonProperty("dateFin")
    private Date dateFin;
    @JsonProperty("searchFieldName")
    private Object searchFieldName;
    @JsonProperty("values")
    private List<String> values = null;
    @JsonProperty("type")
    private String type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("title")
    public String getTitle() {
        if(title == null){
            return "";
        }
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("legalStatus")
    public String getLegalStatus() {
        return legalStatus;
    }

    @JsonProperty("legalStatus")
    public void setLegalStatus(String legalStatus) {
        this.legalStatus = legalStatus;
    }

    @JsonProperty("dateVersion")
    public Object getDateVersion() {
        return dateVersion;
    }

    @JsonProperty("dateVersion")
    public void setDateVersion(Object dateVersion) {
        this.dateVersion = dateVersion;
    }

    @JsonProperty("dateDebut")
    public Date getDateDebut() {
        return dateDebut;
    }

    @JsonProperty("dateDebut")
    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    @JsonProperty("dateFin")
    public Date getDateFin() {
        return dateFin;
    }

    @JsonProperty("dateFin")
    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    @JsonProperty("searchFieldName")
    public Object getSearchFieldName() {
        return searchFieldName;
    }

    @JsonProperty("searchFieldName")
    public void setSearchFieldName(Object searchFieldName) {
        this.searchFieldName = searchFieldName;
    }

    @JsonProperty("values")
    public List<String> getValues() {
        return values;
    }

    @JsonProperty("values")
    public void setValues(List<String> values) {
        this.values = values;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}