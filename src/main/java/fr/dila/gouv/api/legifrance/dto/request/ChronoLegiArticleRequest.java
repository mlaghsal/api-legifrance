package fr.dila.gouv.api.legifrance.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "elementCid",
        "textCid"
})
public class ChronoLegiArticleRequest {
    @JsonProperty("elementCid")
    private String elementCid = "LEGIARTI000006070721";
    @JsonProperty("textCid")
    private String textCid = "LEGITEXT000006070721";

    @JsonProperty("elementCid")
    public String getElementCid() {
        return elementCid;
    }

    @JsonProperty("elementCid")
    public void setElementCid(String elementCid) {
        this.elementCid = elementCid;
    }

    @JsonProperty("textCid")
    public String getTextCid() {
        return textCid;
    }

    @JsonProperty("textCid")
    public void setTextCid(String textCid) {
        this.textCid = textCid;
    }
}
