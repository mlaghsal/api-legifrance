package fr.dila.gouv.api.legifrance.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "textCid"
})
public class JorfConsultRequest {
    @JsonProperty("textCid")
    private String textCid = "JORFTEXT000033736934";

    @JsonProperty("textCid")
    public String getTextCid() {
        return textCid;
    }

    @JsonProperty("textCid")
    public void setTextCid(String textCid) {
        this.textCid = textCid;
    }
}
