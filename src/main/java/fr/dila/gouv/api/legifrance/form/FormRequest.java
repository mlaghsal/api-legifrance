package fr.dila.gouv.api.legifrance.form;

import fr.dila.gouv.api.legifrance.dto.request.*;

public class FormRequest<T> {

    private String pathApi = null;

    /**
     *
     * @return le path de l'api
     */
    public String getPathApi() {
        return pathApi;
    }

    /**
     *
     * @param pathApi path de l'api
     */
    public void setPathApi(String pathApi) {
        this.pathApi = pathApi;
    }

    /**
     * On construit le request de l'api selon le type
     * @param form
     * @return
     */
    public T getRequest(FormContent form){
        if("texteJORF".equals(form.getType())){
            final JorfConsultRequest request = new JorfConsultRequest();
            request.setTextCid(form.getId());
            this.setPathApi("/consult/jorf");
            return (T) request;
        }else if("texteLODA".equals(form.getType())){
            final LawDecreeConsultRequest request = new LawDecreeConsultRequest();
            request.setTextId(form.getId());
            request.setDate(form.getDate());
            this.setPathApi("/consult/lawDecree");
            return (T) request;
        }else if("texteLEGI".equals(form.getType())){
            final LegiConsultRequest request = new LegiConsultRequest();
            request.setTextId(form.getId());
            request.setDate(form.getDate());
            this.setPathApi("/consult/legiPart");
            return (T) request;
        }else if("accord".equals(form.getType())){
            final AccoConsultRequest request = new AccoConsultRequest();
            request.setId(form.getId());
            this.setPathApi("/consult/acco");
            return (T) request;
        }else if("juri".equals(form.getType())){
            final JuriConsultRequest request = new JuriConsultRequest();
            request.setTextId(form.getId());
            this.setPathApi("/consult/juri");
            return (T) request;
        }else if("kali".equals(form.getType())){
            final KaliTextConsultRequest request = new KaliTextConsultRequest();
            request.setId(form.getId());
            this.setPathApi("/consult/kaliText");
            return (T) request;
        }else if("kaliArticle".equals(form.getType())){
            final KaliTextConsultRequest request = new KaliTextConsultRequest();
            request.setId(form.getId());
            this.setPathApi("/consult/kaliArticle");
            return (T) request;
        }else if("article".equals(form.getType())){
            final ArticleRequest request = new ArticleRequest();
            request.setId(form.getId());
            this.setPathApi("/consult/getArticle");
            return (T) request;
        }else if("tdmCode".equals(form.getType())){
            final CodeConsultRequest request = new CodeConsultRequest();
            request.setTextId(form.getId());
            request.setSctId(form.getSectionId());
            request.setDate(form.getDate());
            this.setPathApi("/consult/code/tableMatieres");
            return (T) request;
        }else if("code".equals(form.getType())){
            final CodeConsultRequest request = new CodeConsultRequest();
            request.setTextId(form.getId());
            request.setSctId(form.getSectionId());
            request.setDate(form.getDate());
            this.setPathApi("/consult/code");
            return (T) request;
        }else if("circ".equals(form.getType())){
            final CirculaireConsultRequest request = new CirculaireConsultRequest();
            request.setId(form.getId());
            this.setPathApi("/consult/circulaire");
            return (T) request;
        }else if("cnil".equals(form.getType())){
            final CnilConsultRequest request = new CnilConsultRequest();
            request.setTextId(form.getId());
            this.setPathApi("/consult/cnil");
            return (T) request;
        }else {
            return null;
        }
    }
}