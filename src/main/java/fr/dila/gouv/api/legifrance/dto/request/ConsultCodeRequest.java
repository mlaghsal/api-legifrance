package fr.dila.gouv.api.legifrance.dto.request;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "date",
        "sctCid",
        "textId"
})
public class ConsultCodeRequest implements Serializable {
    private final static long serialVersionUID = 8038496023072255596L;

    /**
     * Date de consultation
     */
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    @JsonProperty("date")
    private Date date = new Date();

    /**
     * Chronical ID de la section a consulter
     */
    @JsonProperty("sctCid")
    private String sctId = "LEGISCTA000006132305";

    /**
     * Identifiant ID du texte
     */
    @JsonProperty("textId")
    private String textId = "LEGITEXT000006075116";

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSctId() {
        return sctId;
    }

    public void setSctId(String sctId) {
        this.sctId = sctId;
    }

    public String getTextId() {
        return textId;
    }

    public void setTextId(String textId) {
        this.textId = textId;
    }
}
