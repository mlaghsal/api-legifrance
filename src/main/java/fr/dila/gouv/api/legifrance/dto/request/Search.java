package fr.dila.gouv.api.legifrance.dto.request;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "champs",
        "filtres",
        "operateur",
        "pageNumber",
        "pageSize",
        "sort",
        "typePagination"
})
public class Search {
    @JsonProperty("champs")
    private List<Champ> champs = null;
    @JsonProperty("filtres")
    private List<FiltreSearch> filtres = null;
    @JsonProperty("operateur")
    private String operateur = "ET";
    @JsonProperty("pageNumber")
    private Integer pageNumber;
    @JsonProperty("pageSize")
    private Integer pageSize;
    @JsonProperty("sort")
    private String sort;
    @JsonProperty("typePagination")
    private String typePagination;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("champs")
    public List<Champ> getChamps() {
        return champs;
    }

    @JsonProperty("champs")
    public void setChamps(List<Champ> champs) {
        this.champs = champs;
    }

    @JsonProperty("filtres")
    public List<FiltreSearch> getFiltres() {
        return filtres;
    }

    @JsonProperty("filtres")
    public void setFiltres(List<FiltreSearch> filtres) {
        this.filtres = filtres;
    }

    @JsonProperty("operateur")
    public String getOperateur() {
        return operateur;
    }

    @JsonProperty("operateur")
    public void setOperateur(String operateur) {
        this.operateur = operateur;
    }

    @JsonProperty("pageNumber")
    public Integer getPageNumber() {
        return pageNumber;
    }

    @JsonProperty("pageNumber")
    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    @JsonProperty("pageSize")
    public Integer getPageSize() {
        return pageSize;
    }

    @JsonProperty("pageSize")
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @JsonProperty("sort")
    public String getSort() {
        return sort;
    }

    @JsonProperty("sort")
    public void setSort(String sort) {
        this.sort = sort;
    }

    @JsonProperty("typePagination")
    public String getTypePagination() {
        return typePagination;
    }

    @JsonProperty("typePagination")
    public void setTypePagination(String typePagination) {
        this.typePagination = typePagination;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
