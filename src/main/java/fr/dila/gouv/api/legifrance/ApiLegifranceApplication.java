package fr.dila.gouv.api.legifrance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

@SpringBootApplication
//@ApplicationPath("/api-legifrance")
public class ApiLegifranceApplication extends SpringBootServletInitializer  implements ApplicationRunner  {

    private static final Logger logger = LoggerFactory.getLogger(ApiLegifranceApplication.class);

    @Value("${proxy.hostname}")
    private String proxyHost;

    @Value("${proxy.port:8080}")
    private String proxyPort;

    @Value("${trustStore}")
    private String trustStore;

    @Value("${trustStorePassword}")
    private String trustStorePassword;


    /**
     *
     * @param application
     * @return
     */
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
    {
        SpringApplicationBuilder builder = application.sources(new Class[] { ApiLegifranceApplication.class });
        return builder;
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(ApiLegifranceApplication.class, args);
    }

    /**
     *
     * @param applicationArguments
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments applicationArguments) throws IOException {

        /**
        * Copier le trustStore à coté du war pour qu'il soit reconu par le System.setProperty
         */
        FileCopyUtils.copy(new ClassPathResource(trustStore).getInputStream(),
                new FileOutputStream(trustStore));

        /**
         * Test si on utilise un proxy
         */
        if(this.proxyHost != null && !this.proxyHost.trim().isEmpty()) {
            Properties props = System.getProperties();
            props.put("http.proxyHost", proxyHost);
            props.put("http.proxyPort", proxyPort);

            props.put("https.proxyHost", proxyHost);
            props.put("https.proxyPort", proxyPort);

            props.put("java.net.useSystemProxies", "true");
        }

        /**
         * setter le trustStore (certificat de la plate forme PISTE aife_economie_gouv_fr.crt)
         */
        System.setProperty("javax.net.ssl.trustStore", trustStore);
        System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);
        System.setProperty("javax.net.ssl.trustStoreType", "JKS");
        //System.setProperty("javax.net.debug", "ssl");
    }

}

