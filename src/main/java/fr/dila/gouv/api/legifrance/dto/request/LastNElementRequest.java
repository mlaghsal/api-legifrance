package fr.dila.gouv.api.legifrance.dto.request;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "nbElement"
})
public class LastNElementRequest {
    @JsonProperty("nbElement")
    private Integer nbElement = 5;

    @JsonProperty("nbElement")
    public Integer getNbElement() {
        return nbElement;
    }

    @JsonProperty("nbElement")
    public void setNbElement(Integer nbElement) {
        this.nbElement = nbElement;
    }
}
