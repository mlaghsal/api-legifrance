package fr.dila.gouv.api.legifrance.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id"
})
public class CnilConsultRequest {
    @JsonProperty("textId")
    private String textId = "CNILTEXT000017652361";

    @JsonProperty("textId")
    public String getTextId() {
        return textId;
    }

    @JsonProperty("textId")
    public void setTextId(String textId) {
        this.textId = textId;
    }

}
