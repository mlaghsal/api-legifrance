package fr.dila.gouv.api.legifrance.dto.request;
import java.util.Arrays;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "keyWords",
        "legalStatus",
        "pageNumber",
        "pageSize",
        "searchValue",
        "sort"
})
public class ConventionsListRequest {
    @JsonProperty("keyWords")
    private List<String> keyWords = Arrays.asList("ABATTOIRS", "CHAUX HYDRAULIQUES");
    @JsonProperty("legalStatus")
    private List<String> legalStatus = Arrays.asList("VIGUEUR", "ABROGE", "VIGUEUR_DIFF");
    @JsonProperty("pageNumber")
    private Integer pageNumber = 1;
    @JsonProperty("pageSize")
    private Integer pageSize = 10;
    @JsonProperty("searchValue")
    private String searchValue = "Industrie";
    @JsonProperty("sort")
    private String sort = "DATE_PUBLI_ASC";

    @JsonProperty("keyWords")
    public List<String> getKeyWords() {
        return keyWords;
    }

    @JsonProperty("keyWords")
    public void setKeyWords(List<String> keyWords) {
        this.keyWords = keyWords;
    }

    @JsonProperty("legalStatus")
    public List<String> getLegalStatus() {
        return legalStatus;
    }

    @JsonProperty("legalStatus")
    public void setLegalStatus(List<String> legalStatus) {
        this.legalStatus = legalStatus;
    }

    @JsonProperty("pageNumber")
    public Integer getPageNumber() {
        return pageNumber;
    }

    @JsonProperty("pageNumber")
    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    @JsonProperty("pageSize")
    public Integer getPageSize() {
        return pageSize;
    }

    @JsonProperty("pageSize")
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @JsonProperty("searchValue")
    public String getSearchValue() {
        return searchValue;
    }

    @JsonProperty("searchValue")
    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    @JsonProperty("sort")
    public String getSort() {
        return sort;
    }

    @JsonProperty("sort")
    public void setSort(String sort) {
        this.sort = sort;
    }
}
