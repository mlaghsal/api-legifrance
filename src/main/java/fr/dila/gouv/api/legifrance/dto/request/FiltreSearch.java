package fr.dila.gouv.api.legifrance.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Date;
import java.util.List;
import java.util.Map;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "facette",
        "valeurs",
        "multiValeurs",
        "dates",
        "singleDate"
})
public class FiltreSearch {

    @JsonProperty("facette")
    private String facette;

    @JsonProperty("valeurs")
    private List<String> valeurs;

    @JsonProperty("multiValeurs")
    private Map<String, List<String>> multiValeurs;

    @JsonProperty("dates")
    private DatePeriod dates;

    @JsonProperty("singleDate")
    private Date singleDate;

    @JsonProperty("facette")
    public String getFacette() {
        return facette;
    }

    @JsonProperty("facette")
    public void setFacette(String facette) {
        this.facette = facette;
    }

    @JsonProperty("valeurs")
    public List<String> getValeurs() {
        return valeurs;
    }

    @JsonProperty("valeurs")
    public void setValeurs(List<String> valeurs) {
        this.valeurs = valeurs;
    }

    @JsonProperty("multiValeurs")
    public Map<String, List<String>> getMultiValeurs() {
        return multiValeurs;
    }

    @JsonProperty("multiValeurs")
    public void setMultiValeurs(Map<String, List<String>> multiValeurs) {
        this.multiValeurs = multiValeurs;
    }

    @JsonProperty("dates")
    public DatePeriod getDates() {
        return dates;
    }

    @JsonProperty("dates")
    public void setDates(DatePeriod dates) {
        this.dates = dates;
    }

    @JsonProperty("singleDate")
    public Date getSingleDate() {
        return singleDate;
    }

    @JsonProperty("singleDate")
    public void setSingleDate(Date singleDate) {
        this.singleDate = singleDate;
    }
}
