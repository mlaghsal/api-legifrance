package fr.dila.gouv.api.legifrance.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "articleId"
})
public class ConcordanceLinksRequest {
    @JsonProperty("articleId")
    private String articleId = "LEGIARTI000006419320";

    @JsonProperty("articleId")
    public String getArticleId() {
        return articleId;
    }

    @JsonProperty("articleId")
    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }
}
