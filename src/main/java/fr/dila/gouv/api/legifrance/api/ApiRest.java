package fr.dila.gouv.api.legifrance.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class ApiRest<T> {

    private static final Logger logger = LoggerFactory.getLogger(ApiRest.class);

    @Autowired
    private OAuth2RestOperations restTemplate;

    /**
     *
     * @param request
     * @param apiUrl
     * @return
     */
    public ResponseEntity<String> doPost(T request, final String apiUrl){

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", "Bearer " + getAccessToken() );
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(apiUrl, new HttpEntity<>(request, headers), String.class);
        return responseEntity;
    }

    /**
     *
     * @param apiUrl
     * @return
     */
    public ResponseEntity<String> doGet(final String apiUrl){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", "Bearer " + getAccessToken() );
        ResponseEntity<String> responseEntity = restTemplate.exchange(apiUrl, HttpMethod.GET, new HttpEntity<>(null, headers), String.class);
        return responseEntity;
    }

    /**
     *
     * @return
     */
    public String getAccessToken(){
        final OAuth2AccessToken oAuth2AccessToken = restTemplate.getAccessToken();
        logger.debug("Token : " + oAuth2AccessToken.getValue());
        return oAuth2AccessToken.getValue();
    }
}
