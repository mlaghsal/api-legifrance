package fr.dila.gouv.api.legifrance.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;


@Controller
public class HomeContoller {
    @RequestMapping("/")
    public String home(Map<String, Object> model) {
        model.put("message", "Api Legifrance");
        return "index";
    }
}
