package fr.dila.gouv.api.legifrance.dto.request;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "textId"
})
public class JuriConsultRequest {
    @JsonProperty("textId")
    private String textId = "JURITEXT000037999394";

    @JsonProperty("textId")
    public String getTextId() {
        return textId;
    }

    @JsonProperty("textId")
    public void setTextId(String textId) {
        this.textId = textId;
    }
}
