package fr.dila.gouv.api.legifrance.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "codeName",
        "pageNumber",
        "pageSize",
        "sort",
        "states"
})
public class ListCodesRequest implements Serializable {
    private final static long serialVersionUID = 8038496023072255596L;

    /**
     * Titre de code à chercher
     */
    @JsonProperty("codeName")
    private String codeName = "Code civil";

    /**
     * Numéro de la page à consulter
     */
    @JsonProperty("pageNumber")
    private Integer pageNumber = 1;

    /**
     * Nombre de résultats par page
     */
    @JsonProperty("pageSize")
    private Integer pageSize = 10;

    /**
     * Ordre de tri
     */
    @JsonProperty("sort")
    private String sort = "TITLE_ASC";


    /**
     * Liste des états juridiques à filtrer
     * ["VIGUEUR", "ABROGE", "VIGUEUR_DIFF"]
     */
    @JsonProperty("states")
    private List<String> states = Arrays.asList("VIGUEUR");


    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public List<String> getStates() {
        return states;
    }

    public void setStates(List<String> states) {
        this.states = states;
    }
}
