package fr.dila.gouv.api.legifrance.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "endYear",
        "startYear"
})
public class TableRequest {
    @JsonProperty("endYear")
    private Integer endYear = 2019;
    @JsonProperty("startYear")
    private Integer startYear = 2017;

    @JsonProperty("endYear")
    public Integer getEndYear() {
        return endYear;
    }

    @JsonProperty("endYear")
    public void setEndYear(Integer endYear) {
        this.endYear = endYear;
    }

    @JsonProperty("startYear")
    public Integer getStartYear() {
        return startYear;
    }

    @JsonProperty("startYear")
    public void setStartYear(Integer startYear) {
        this.startYear = startYear;
    }
}
