package fr.dila.gouv.api.legifrance.dto.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "executionTime",
        "results",
        "totalResultNumber",
        "typePagination"
})
public class SearchResponse {

    @JsonProperty("executionTime")
    private Integer executionTime;
    @JsonProperty("results")
    private List<Result> results = null;
    @JsonProperty("totalResultNumber")
    private Integer totalResultNumber;
    @JsonProperty("typePagination")
    private String typePagination;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("executionTime")
    public Integer getExecutionTime() {
        return executionTime;
    }

    @JsonProperty("executionTime")
    public void setExecutionTime(Integer executionTime) {
        this.executionTime = executionTime;
    }

    @JsonProperty("results")
    public List<Result> getResults() {
        return results;
    }

    @JsonProperty("results")
    public void setResults(List<Result> results) {
        this.results = results;
    }

    @JsonProperty("totalResultNumber")
    public Integer getTotalResultNumber() {
        return totalResultNumber;
    }

    @JsonProperty("totalResultNumber")
    public void setTotalResultNumber(Integer totalResultNumber) {
        this.totalResultNumber = totalResultNumber;
    }

    @JsonProperty("typePagination")
    public String getTypePagination() {
        return typePagination;
    }

    @JsonProperty("typePagination")
    public void setTypePagination(String typePagination) {
        this.typePagination = typePagination;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}