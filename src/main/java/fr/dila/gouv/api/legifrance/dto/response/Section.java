package fr.dila.gouv.api.legifrance.dto.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "articles",
        "cid",
        "dateDebut",
        "dateFin",
        "dateModif",
        "dereferenced",
        "etat",
        "executionTime",
        "id",
        "intOrdre",
        "sectionConsultee",
        "sections",
        "title"
})
public class Section {

    @JsonProperty("articles")
    private List<Articles> articles = null;
    @JsonProperty("cid")
    private String cid;
    @JsonProperty("dateDebut")
    private String dateDebut;
    @JsonProperty("dateFin")
    private String dateFin;
    @JsonProperty("dateModif")
    private String dateModif;
    @JsonProperty("dereferenced")
    private Boolean dereferenced;
    @JsonProperty("etat")
    private String etat;
    @JsonProperty("executionTime")
    private Integer executionTime;
    @JsonProperty("id")
    private String id;
    @JsonProperty("intOrdre")
    private Integer intOrdre;
    @JsonProperty("sectionConsultee")
    private Boolean sectionConsultee;
    @JsonProperty("sections")
    private List<Object> sections = null;
    @JsonProperty("title")
    private String title;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("articles")
    public List<Articles> getArticles() {
        return articles;
    }

    @JsonProperty("articles")
    public void setArticles(List<Articles> articles) {
        this.articles = articles;
    }

    @JsonProperty("cid")
    public String getCid() {
        return cid;
    }

    @JsonProperty("cid")
    public void setCid(String cid) {
        this.cid = cid;
    }

    @JsonProperty("dateDebut")
    public String getDateDebut() {
        return dateDebut;
    }

    @JsonProperty("dateDebut")
    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    @JsonProperty("dateFin")
    public String getDateFin() {
        return dateFin;
    }

    @JsonProperty("dateFin")
    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    @JsonProperty("dateModif")
    public String getDateModif() {
        return dateModif;
    }

    @JsonProperty("dateModif")
    public void setDateModif(String dateModif) {
        this.dateModif = dateModif;
    }

    @JsonProperty("dereferenced")
    public Boolean getDereferenced() {
        return dereferenced;
    }

    @JsonProperty("dereferenced")
    public void setDereferenced(Boolean dereferenced) {
        this.dereferenced = dereferenced;
    }

    @JsonProperty("etat")
    public String getEtat() {
        return etat;
    }

    @JsonProperty("etat")
    public void setEtat(String etat) {
        this.etat = etat;
    }

    @JsonProperty("executionTime")
    public Integer getExecutionTime() {
        return executionTime;
    }

    @JsonProperty("executionTime")
    public void setExecutionTime(Integer executionTime) {
        this.executionTime = executionTime;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("intOrdre")
    public Integer getIntOrdre() {
        return intOrdre;
    }

    @JsonProperty("intOrdre")
    public void setIntOrdre(Integer intOrdre) {
        this.intOrdre = intOrdre;
    }

    @JsonProperty("sectionConsultee")
    public Boolean getSectionConsultee() {
        return sectionConsultee;
    }

    @JsonProperty("sectionConsultee")
    public void setSectionConsultee(Boolean sectionConsultee) {
        this.sectionConsultee = sectionConsultee;
    }

    @JsonProperty("sections")
    public List<Object> getSections() {
        return sections;
    }

    @JsonProperty("sections")
    public void setSections(List<Object> sections) {
        this.sections = sections;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}