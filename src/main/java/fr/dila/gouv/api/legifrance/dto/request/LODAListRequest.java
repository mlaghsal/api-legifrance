package fr.dila.gouv.api.legifrance.dto.request;
import java.util.Arrays;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "legalStatus",
        "natures",
        "pageNumber",
        "pageSize",
        "publicationDate",
        "signatureDate",
        "sort"
})
public class LODAListRequest {
    @JsonProperty("legalStatus")
    private List<String> legalStatus = Arrays.asList("VIGUEUR", "ABROGE", "VIGUEUR_DIFF");
    @JsonProperty("natures")
    private List<String> natures = Arrays.asList("LOI", "ORDONNANCE", "DECRET");
    @JsonProperty("pageNumber")
    private Integer pageNumber = 1;
    @JsonProperty("pageSize")
    private Integer pageSize = 10;
    @JsonProperty("publicationDate")
    private DatePeriod publicationDate = new DatePeriod("2019-12-31", "2016-01-01");
    @JsonProperty("signatureDate")
    private DatePeriod signatureDate = new DatePeriod("2019-12-31", "2016-01-01");;
    @JsonProperty("sort")
    private String sort = "DATE_PUBLI_ASC";

    @JsonProperty("legalStatus")
    public List<String> getLegalStatus() {
        return legalStatus;
    }

    @JsonProperty("legalStatus")
    public void setLegalStatus(List<String> legalStatus) {
        this.legalStatus = legalStatus;
    }

    @JsonProperty("natures")
    public List<String> getNatures() {
        return natures;
    }

    @JsonProperty("natures")
    public void setNatures(List<String> natures) {
        this.natures = natures;
    }

    @JsonProperty("pageNumber")
    public Integer getPageNumber() {
        return pageNumber;
    }

    @JsonProperty("pageNumber")
    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    @JsonProperty("pageSize")
    public Integer getPageSize() {
        return pageSize;
    }

    @JsonProperty("pageSize")
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @JsonProperty("publicationDate")
    public DatePeriod getPublicationDate() {
        return publicationDate;
    }

    @JsonProperty("publicationDate")
    public void setPublicationDate(DatePeriod publicationDate) {
        this.publicationDate = publicationDate;
    }

    @JsonProperty("signatureDate")
    public DatePeriod getSignatureDate() {
        return signatureDate;
    }

    @JsonProperty("signatureDate")
    public void setSignatureDate(DatePeriod signatureDate) {
        this.signatureDate = signatureDate;
    }

    @JsonProperty("sort")
    public String getSort() {
        return sort;
    }

    @JsonProperty("sort")
    public void setSort(String sort) {
        this.sort = sort;
    }
}
