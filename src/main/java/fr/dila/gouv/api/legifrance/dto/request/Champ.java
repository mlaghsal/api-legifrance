package fr.dila.gouv.api.legifrance.dto.request;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "criteres",
        "operateur",
        "typeChamp"
})
public class Champ {

    @JsonProperty("criteres")
    private List<Critere> criteres = new ArrayList<>();
    @JsonProperty("operateur")
    private String operateur = "ET";
    @JsonProperty("typeChamp")
    private String typeChamp;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("criteres")
    public List<Critere> getCriteres() {
        return criteres;
    }

    @JsonProperty("criteres")
    public void setCriteres(List<Critere> criteres) {
        this.criteres = criteres;
    }

    @JsonProperty("operateur")
    public String getOperateur() {
        return operateur;
    }

    @JsonProperty("operateur")
    public void setOperateur(String operateur) {
        this.operateur = operateur;
    }

    @JsonProperty("typeChamp")
    public String getTypeChamp() {
        return typeChamp;
    }

    @JsonProperty("typeChamp")
    public void setTypeChamp(String typeChamp) {
        this.typeChamp = typeChamp;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}