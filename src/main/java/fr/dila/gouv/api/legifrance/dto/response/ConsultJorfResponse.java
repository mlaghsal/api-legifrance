package fr.dila.gouv.api.legifrance.dto.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "alias",
        "articles",
        "cid",
        "dateDebutVersion",
        "dateFinVersion",
        "dateParution",
        "dereferenced",
        "eli",
        "etat",
        "executionTime",
        "id",
        "idConteneur",
        "inap",
        "jorfText",
        "jurisDate",
        "jurisState",
        "modifDate",
        "nature",
        "nor",
        "nota",
        "notice",
        "numParution",
        "observations",
        "prepWork",
        "rectificatif",
        "resume",
        "sections",
        "signers",
        "textAbroge",
        "textNumber",
        "title",
        "visa"
})
public class ConsultJorfResponse {

    @JsonProperty("alias")
    private String alias;
    @JsonProperty("articles")
    private List<Article> articles = null;
    @JsonProperty("cid")
    private String cid;
    @JsonProperty("dateDebutVersion")
    private String dateDebutVersion;
    @JsonProperty("dateFinVersion")
    private String dateFinVersion;
    @JsonProperty("dateParution")
    private String dateParution;
    @JsonProperty("dereferenced")
    private Boolean dereferenced;
    @JsonProperty("eli")
    private String eli;
    @JsonProperty("etat")
    private String etat;
    @JsonProperty("executionTime")
    private Integer executionTime;
    @JsonProperty("id")
    private String id;
    @JsonProperty("idConteneur")
    private String idConteneur;
    @JsonProperty("inap")
    private Boolean inap;
    @JsonProperty("jorfText")
    private String jorfText;
    @JsonProperty("jurisDate")
    private String jurisDate;
    @JsonProperty("jurisState")
    private String jurisState;
    @JsonProperty("modifDate")
    private String modifDate;
    @JsonProperty("nature")
    private String nature;
    @JsonProperty("nor")
    private String nor;
    @JsonProperty("nota")
    private String nota;
    @JsonProperty("notice")
    private String notice;
    @JsonProperty("numParution")
    private String numParution;
    @JsonProperty("observations")
    private String observations;
    @JsonProperty("prepWork")
    private String prepWork;
    @JsonProperty("rectificatif")
    private String rectificatif;
    @JsonProperty("resume")
    private String resume;
    @JsonProperty("sections")
    private List<Section> sections = null;
    @JsonProperty("signers")
    private String signers;
    @JsonProperty("textAbroge")
    private Boolean textAbroge;
    @JsonProperty("textNumber")
    private String textNumber;
    @JsonProperty("title")
    private String title;
    @JsonProperty("visa")
    private String visa;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("alias")
    public String getAlias() {
        return alias;
    }

    @JsonProperty("alias")
    public void setAlias(String alias) {
        this.alias = alias;
    }

    @JsonProperty("articles")
    public List<Article> getArticles() {
        return articles;
    }

    @JsonProperty("articles")
    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    @JsonProperty("cid")
    public String getCid() {
        return cid;
    }

    @JsonProperty("cid")
    public void setCid(String cid) {
        this.cid = cid;
    }

    @JsonProperty("dateDebutVersion")
    public String getDateDebutVersion() {
        return dateDebutVersion;
    }

    @JsonProperty("dateDebutVersion")
    public void setDateDebutVersion(String dateDebutVersion) {
        this.dateDebutVersion = dateDebutVersion;
    }

    @JsonProperty("dateFinVersion")
    public String getDateFinVersion() {
        return dateFinVersion;
    }

    @JsonProperty("dateFinVersion")
    public void setDateFinVersion(String dateFinVersion) {
        this.dateFinVersion = dateFinVersion;
    }

    @JsonProperty("dateParution")
    public String getDateParution() {
        return dateParution;
    }

    @JsonProperty("dateParution")
    public void setDateParution(String dateParution) {
        this.dateParution = dateParution;
    }

    @JsonProperty("dereferenced")
    public Boolean getDereferenced() {
        return dereferenced;
    }

    @JsonProperty("dereferenced")
    public void setDereferenced(Boolean dereferenced) {
        this.dereferenced = dereferenced;
    }

    @JsonProperty("eli")
    public String getEli() {
        return eli;
    }

    @JsonProperty("eli")
    public void setEli(String eli) {
        this.eli = eli;
    }

    @JsonProperty("etat")
    public String getEtat() {
        return etat;
    }

    @JsonProperty("etat")
    public void setEtat(String etat) {
        this.etat = etat;
    }

    @JsonProperty("executionTime")
    public Integer getExecutionTime() {
        return executionTime;
    }

    @JsonProperty("executionTime")
    public void setExecutionTime(Integer executionTime) {
        this.executionTime = executionTime;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("idConteneur")
    public String getIdConteneur() {
        return idConteneur;
    }

    @JsonProperty("idConteneur")
    public void setIdConteneur(String idConteneur) {
        this.idConteneur = idConteneur;
    }

    @JsonProperty("inap")
    public Boolean getInap() {
        return inap;
    }

    @JsonProperty("inap")
    public void setInap(Boolean inap) {
        this.inap = inap;
    }

    @JsonProperty("jorfText")
    public String getJorfText() {
        return jorfText;
    }

    @JsonProperty("jorfText")
    public void setJorfText(String jorfText) {
        this.jorfText = jorfText;
    }

    @JsonProperty("jurisDate")
    public String getJurisDate() {
        return jurisDate;
    }

    @JsonProperty("jurisDate")
    public void setJurisDate(String jurisDate) {
        this.jurisDate = jurisDate;
    }

    @JsonProperty("jurisState")
    public String getJurisState() {
        return jurisState;
    }

    @JsonProperty("jurisState")
    public void setJurisState(String jurisState) {
        this.jurisState = jurisState;
    }

    @JsonProperty("modifDate")
    public String getModifDate() {
        return modifDate;
    }

    @JsonProperty("modifDate")
    public void setModifDate(String modifDate) {
        this.modifDate = modifDate;
    }

    @JsonProperty("nature")
    public String getNature() {
        return nature;
    }

    @JsonProperty("nature")
    public void setNature(String nature) {
        this.nature = nature;
    }

    @JsonProperty("nor")
    public String getNor() {
        return nor;
    }

    @JsonProperty("nor")
    public void setNor(String nor) {
        this.nor = nor;
    }

    @JsonProperty("nota")
    public String getNota() {
        return nota;
    }

    @JsonProperty("nota")
    public void setNota(String nota) {
        this.nota = nota;
    }

    @JsonProperty("notice")
    public String getNotice() {
        return notice;
    }

    @JsonProperty("notice")
    public void setNotice(String notice) {
        this.notice = notice;
    }

    @JsonProperty("numParution")
    public String getNumParution() {
        return numParution;
    }

    @JsonProperty("numParution")
    public void setNumParution(String numParution) {
        this.numParution = numParution;
    }

    @JsonProperty("observations")
    public String getObservations() {
        return observations;
    }

    @JsonProperty("observations")
    public void setObservations(String observations) {
        this.observations = observations;
    }

    @JsonProperty("prepWork")
    public String getPrepWork() {
        return prepWork;
    }

    @JsonProperty("prepWork")
    public void setPrepWork(String prepWork) {
        this.prepWork = prepWork;
    }

    @JsonProperty("rectificatif")
    public String getRectificatif() {
        return rectificatif;
    }

    @JsonProperty("rectificatif")
    public void setRectificatif(String rectificatif) {
        this.rectificatif = rectificatif;
    }

    @JsonProperty("resume")
    public String getResume() {
        return resume;
    }

    @JsonProperty("resume")
    public void setResume(String resume) {
        this.resume = resume;
    }

    @JsonProperty("sections")
    public List<Section> getSections() {
        return sections;
    }

    @JsonProperty("sections")
    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    @JsonProperty("signers")
    public String getSigners() {
        return signers;
    }

    @JsonProperty("signers")
    public void setSigners(String signers) {
        this.signers = signers;
    }

    @JsonProperty("textAbroge")
    public Boolean getTextAbroge() {
        return textAbroge;
    }

    @JsonProperty("textAbroge")
    public void setTextAbroge(Boolean textAbroge) {
        this.textAbroge = textAbroge;
    }

    @JsonProperty("textNumber")
    public String getTextNumber() {
        return textNumber;
    }

    @JsonProperty("textNumber")
    public void setTextNumber(String textNumber) {
        this.textNumber = textNumber;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("visa")
    public String getVisa() {
        return visa;
    }

    @JsonProperty("visa")
    public void setVisa(String visa) {
        this.visa = visa;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}