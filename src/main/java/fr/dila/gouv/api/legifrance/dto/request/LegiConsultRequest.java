package fr.dila.gouv.api.legifrance.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "date",
        "textId"
})
public class LegiConsultRequest {
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    @JsonProperty("date")
    private Date date = new Date();
    @JsonProperty("textId")
    private String textId = "JORFTEXT000000882738";

    @DateTimeFormat(pattern = "yyyy-mm-dd")
    @JsonProperty("date")
    public Date getDate() {
        return date;
    }

    @DateTimeFormat(pattern = "yyyy-mm-dd")
    @JsonProperty("date")
    public void setDate(Date date) {
        this.date = date;
    }

    @JsonProperty("textId")
    public String getTextId() {
        return textId;
    }

    @JsonProperty("textId")
    public void setTextId(String textId) {
        this.textId = textId;
    }
}
