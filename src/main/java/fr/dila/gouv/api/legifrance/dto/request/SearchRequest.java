package fr.dila.gouv.api.legifrance.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "fond",
        "recherche"
})
public class SearchRequest {
    @JsonProperty("fond")
    private String fond;

    @JsonProperty("recherche")
    private Search search;

    @JsonProperty("fond")
    public String getFond() {
        return fond;
    }
    @JsonProperty("fond")
    public void setFond(String fond) {
        this.fond = fond;
    }

    @JsonProperty("recherche")
    public Search getSearch() {
        return search;
    }
    @JsonProperty("recherche")
    public void setSearch(Search search) {
        this.search = search;
    }
}
