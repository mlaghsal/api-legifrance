package fr.dila.gouv.api.legifrance;

import fr.dila.gouv.api.legifrance.services.LegiService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApiLegifranceApplicationTest {

    private static final Logger logger = LoggerFactory.getLogger(ApiLegifranceApplicationTest.class);

    @Autowired
    private LegiService service;

    @Value("${secured.service.url}")
    private String endpoint;


    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void contextLoads() {
    }

}